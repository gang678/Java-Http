package com.gxg.server.controller;

import com.gxg.server.service.FileService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Controller
public class FileController {
    @Resource
    private FileService fileService;

    //预览控制器
    @GetMapping("/toEditor")
    public String toEditor(){
        return "upload/uploadfile";
    }

    //文件上传
    @PostMapping(value = "/file/upload",produces = "application/json")
    @ResponseBody
    public Map<String,Object> fileUpload(@RequestParam(value = "editormd-image-file", required = true) MultipartFile file, HttpServletRequest request) throws IOException {
        //上传路径保存设置

        //获得SpringBoot当前项目的路径：System.getProperty("user.dir")
        String path = System.getProperty("user.dir")+"/Java-Http/upload/";

        //按照月份进行分类：
        Calendar instance = Calendar.getInstance();
        String   month    = (instance.get(Calendar.MONTH) + 1)+"-month";
        path = path+month;

        File realPath = new File(path);
        if (!realPath.exists()){
            realPath.mkdirs();
        }

        //上传文件地址
        System.out.println("上传文件保存地址："+realPath);

        //解决文件名字问题：我们使用uuid;
        String filename = "pg-"+ UUID.randomUUID().toString().replaceAll("-", "")+".jpg";
        File newfile = new File(realPath, filename);
        file.transferTo(newfile);

        System.out.println("============上传成功");
        //给editormd进行回调
        Map<String,Object> map = new HashMap<>();
        map.put("url","/upload/"+month+"/"+ filename);
        map.put("success", 1);
        map.put("message", "upload success!");
        System.out.println("============上传完成");
        return map;
    }
}
