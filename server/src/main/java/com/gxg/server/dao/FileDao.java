package com.gxg.server.dao;

import com.gxg.server.bean.FileSource;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface FileDao {
    //查询
    List<FileSource> queryFileSource();
    //新增
    int addFileSource(FileSource fileSource);
    //根据id查询
    FileSource getFileSourceById(int id);
    //根据id删除
    int deleteFileSourceById(int id);
}
