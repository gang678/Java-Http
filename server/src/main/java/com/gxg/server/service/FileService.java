package com.gxg.server.service;

import com.gxg.server.bean.FileSource;

import java.util.List;

public interface FileService {
    //查询
    List<FileSource> queryFileSource();
    //新增
    int addFileSource(FileSource fileSource);
    //根据id查询
    FileSource getFileSourceById(int id);
    //根据id删除
    int deleteFileSourceById(int id);
}
