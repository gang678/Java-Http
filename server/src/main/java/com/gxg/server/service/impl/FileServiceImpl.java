package com.gxg.server.service.impl;

import com.gxg.server.bean.FileSource;
import com.gxg.server.dao.FileDao;
import com.gxg.server.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FileServiceImpl implements FileService {

    @Autowired(required = false)
    private FileDao fileDao;

    @Override
    public List<FileSource> queryFileSource() {
        return fileDao.queryFileSource();
    }

    @Override
    public int addFileSource(FileSource fileSource) {
        return fileDao.addFileSource(fileSource);
    }

    @Override
    public FileSource getFileSourceById(int id) {
        return fileDao.getFileSourceById(id);
    }

    @Override
    public int deleteFileSourceById(int id) {
        return fileDao.deleteFileSourceById(id);
    }
}
