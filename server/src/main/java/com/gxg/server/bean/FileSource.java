package com.gxg.server.bean;

import lombok.*;
import org.springframework.stereotype.Component;

import java.util.Date;

@EqualsAndHashCode
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Component
public class FileSource {
    private Integer id;
    private String fileSize;
    private String fileType;
    private String fileName;
    private Date createTime;
}
